import { Injectable } from '@angular/core';
import { LogFormatterService } from '../../../../../../libs/logger-lib/src/lib/log-formatter.service';

@Injectable({
  providedIn: 'root'
})
export class CustomLogFormatterService implements LogFormatterService {
  format(message: string): string {
    const now = new Date().toISOString();
    return `[${now}] ${message}`;
  }

}
