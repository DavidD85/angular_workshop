import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AirportService {

  constructor(private http: HttpClient) {
  }

  public findAll(): Observable<string[]> {
    return this.http.get<string[]>('http://www.angular.at/api/airport');
  }
}
