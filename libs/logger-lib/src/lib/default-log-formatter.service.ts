import { Injectable } from '@angular/core';
import { LogFormatterService } from './log-formatter.service';

@Injectable({
  providedIn: 'root'
})
export class DefaultLogFormatterService extends LogFormatterService {

  format(message: string): string {
    return message;
  }
}
