import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Passenger } from '@flight-workspace/passenger/domain';
import { Observable } from 'rxjs';
import { Flight } from '../models/passengers';

@Injectable({
  providedIn: 'root'
})
export class PassengerService {

  url = 'http://www.angular.at/api/passenger';

  constructor(private http: HttpClient) {
  }

  getAllPassengers(): Observable<Passenger> {

    return this.http.get<Passenger>(this.url);
  }

  addPassenger(passenger: Passenger): void {
    this.http.post(this.url, passenger);
  }
}
