export interface Flight {
  id: string;
  bonusMiles: number;
  firstName: string;
  name: string;
  passengerStatus: string;
}
